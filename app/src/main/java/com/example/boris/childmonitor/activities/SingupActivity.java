package com.example.boris.childmonitor.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.boris.childmonitor.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

public class SingupActivity extends AppCompatActivity implements  View.OnClickListener{

    private EditText emailRegistro;
    private EditText contraseñaRegistro;
    private EditText contraseña2Registro;
    private Button botonRegistro;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_singup);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context=this;
        toolbar.setTitle("Child");
        FirebaseApp.initializeApp(context);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        firebaseAuth= FirebaseAuth.getInstance();
        emailRegistro=(EditText)findViewById(R.id.emil_registro);
        contraseña2Registro=(EditText)findViewById(R.id.contraseña2_registro);
        contraseñaRegistro=(EditText)findViewById(R.id.contraseña_registro);
        botonRegistro=(Button)findViewById(R.id.boton_registro);
        botonRegistro.setOnClickListener(this );
    }
    public void onStart() {
        super.onStart();


    }


    @Override
    public void onClick(View v) {
        if(v==botonRegistro){
            registrarUsuario();
        }
    }
    private void registrarUsuario()
    {

        String emailU =emailRegistro.getText().toString();
        String passwordU = contraseñaRegistro.getText().toString();
        String password2U= contraseña2Registro.getText().toString();

        if(TextUtils.isEmpty(emailU)){
            //email esta vacio
            Toast.makeText(this, "Introduzca un email", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(passwordU)){
            //contraseña vacia
            Toast.makeText(this, "Introduzca una contaseña", Toast.LENGTH_SHORT).show();
            return;
        }
        if(password2U.compareTo(passwordU)!=0){
            Toast.makeText(this, "Las contraseñas no coinciden",Toast.LENGTH_SHORT).show();
            return;
        }
        //todoo en orden
        firebaseAuth.createUserWithEmailAndPassword(emailU,passwordU).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(SingupActivity.this, "Lo lograste",Toast.LENGTH_SHORT).show();
                    finish();
                    Intent i = new Intent(SingupActivity.this, PerfilActivity.class );
                    startActivity(i);

                    
                }
                else{
                    Toast.makeText(SingupActivity.this, "Revisa que la contraseña tenga al menos 8 caractéres, una mayúscula y un número",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void guardarInformacion() {

    }
}
