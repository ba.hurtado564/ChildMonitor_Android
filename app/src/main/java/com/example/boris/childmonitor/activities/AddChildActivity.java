package com.example.boris.childmonitor.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.boris.childmonitor.R;
import com.example.boris.childmonitor.clases.InfoHijo;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;

import static android.provider.MediaStore.Images.Media.getBitmap;

public class AddChildActivity extends AppCompatActivity {

    private static final int REQUEST_PHOTO =2222 ;
    private Spinner edad;
    private EditText edadSeleccionada;
    private Uri fotoSubida;
    private TextView subirFoto;
    private Button agregarHijo;
    private FirebaseAuth firebaseAuth;
    private EditText nombreHijo;
    private DatabaseReference databaseReference;
    private long size=0;
    private EditText prueba;
    String edadFinal ;
    private Switch smartphone;
    private RadioGroup radioGroup;
    private RadioButton radioButton;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_child);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        firebaseAuth=FirebaseAuth.getInstance();
        databaseReference= FirebaseDatabase.getInstance().getReference();
        agregarHijo=(Button)findViewById(R.id.agregarHijo);
        smartphone=(Switch)findViewById(R.id.smartphone);
        radioGroup=(RadioGroup)findViewById(R.id.radioGroup);



        agregarHijo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarInfoHijo();

            }
        });
        edadSeleccionada=(EditText)findViewById(R.id.edad_seleccionada);
        edad=(Spinner)findViewById(R.id.seleccionar_edad);
        subirFoto=(TextView)findViewById(R.id.tomar_foto);
        prueba=(EditText)findViewById(R.id.edad_seleccionada);
        nombreHijo=(EditText)findViewById(R.id.nombre_hijo);
        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(this,R.array.edad_opciones,android.R.layout.simple_spinner_item );
        edad.setAdapter(adapter);
        edad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               edadSeleccionada.setText("Edad "+edad.getItemAtPosition(position).toString());
                String ed=edad.getItemAtPosition(position).toString();
                String []edi=ed.split(" ");
                edadFinal= edi[0];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        subirFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seleccionarImagen();
            }
        });
        databaseReference.child("Hijos").child(firebaseAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                    size=dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void guardarInfoHijo() {
        String padre = firebaseAuth.getCurrentUser().getUid();
        String nombreHijoString= nombreHijo.getText().toString();
        if(TextUtils.isEmpty(nombreHijoString)){
            Toast.makeText(this,"El nombre de su hijo no puede ser vacio",Toast.LENGTH_SHORT).show();
            return;
        }
        int edad1=Integer.parseInt(edadFinal);
        boolean telefono =smartphone.isChecked();

        int selectedId =radioGroup.getCheckedRadioButtonId();

        radioButton=(RadioButton)findViewById(selectedId);
        if(radioButton!=null){
        String monitoreo = radioButton.getText().toString();
        InfoHijo hijo= new InfoHijo(padre,nombreHijoString, edad1,telefono,monitoreo);
        FirebaseUser users  = firebaseAuth.getCurrentUser();

        databaseReference.child(new String("Hijos")).child(users.getUid()).child("hijo"+size).setValue(hijo);
        }
        else{
            Toast.makeText(this,"Debe seleccionar algun tipo de monitoreo",Toast.LENGTH_SHORT).show();
            return;
        }


    }

    private void seleccionarImagen(){
        PackageManager packageManager= getPackageManager();
        if(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)==false){
            Toast.makeText(this,"El dispositivo no tiene camara",Toast.LENGTH_SHORT).show();
            return;
        }
        Intent escogerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        escogerIntent.setType("image/*");
        Intent tomarFotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStorageDirectory(),"/profile.png");
        tomarFotoIntent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(f));
        String escogerTitulo="Tome o seleccione una foto";
        Intent seleccionIntent = Intent.createChooser(escogerIntent,escogerTitulo);
        seleccionIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,new Intent []{tomarFotoIntent});
        fotoSubida=Uri.fromFile(f);
        if(tomarFotoIntent.resolveActivity(getPackageManager())!=null){
            startActivityForResult(seleccionIntent,REQUEST_PHOTO);
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == REQUEST_PHOTO && resultCode == Activity.RESULT_OK) {
            if(data!=null) {
                fotoSubida = data.getData();
            }
            if(fotoSubida != null){
                Uri selectedImage = fotoSubida;
                getContentResolver().notifyChange(selectedImage, null);

                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);

                }
                Bitmap bmp = null;
                try {
                    bmp = getBitmap(getContentResolver(),fotoSubida);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if(bmp != null) {

                    ImageView clientIv = (ImageView)findViewById(R.id.foto_perfil);
                    clientIv.setImageBitmap(bmp);
                    clientIv.setRotation(clientIv.getRotation()+90);
                }
            }else{
                Toast.makeText(this,"Error while capturing Image by Uri",Toast.LENGTH_LONG).show();
            }

        }
    }
   }
