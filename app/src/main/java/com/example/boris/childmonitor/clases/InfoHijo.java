package com.example.boris.childmonitor.clases;

/**
 * Created by Boris Alfredo on 6/10/2017.
 */

public class InfoHijo {
    public String padre;
    public String nombre;
    public int edad;
    public boolean smartphone;
    public String  monitoreo;

    public InfoHijo(String padre, String nombre,int edad,boolean smartphone,String monitoreo) {
        this.padre = padre;
        this.nombre = nombre;
        this.edad=edad;
        this.smartphone=smartphone;
        this.monitoreo=monitoreo;
    }

    public String getPadre() {
        return padre;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }
    public boolean getSmartphone(){
        return smartphone;
    }
    public String getMonitoreo(){
        return monitoreo;
    }

}
