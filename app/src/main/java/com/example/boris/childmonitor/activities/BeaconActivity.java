package com.example.boris.childmonitor.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Region;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.boris.childmonitor.R;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;

import java.util.Collection;

public class BeaconActivity extends AppCompatActivity implements BeaconConsumer {


    protected static final String TAG = "MonitoringActivity";
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private BeaconManager beaconManager;
    String uUID = "523acafc-d72e-4444-bf3f-2a1894cdf5b2";
    int rangeTxt = 1;
    EditText range;
    EditText uuid;
    TextView safeMode;
    double distanceR;
    TextView actualRange;
    TextView tvDistance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon);
        range = (EditText) findViewById(R.id.etSafetyRange);
        uuid = (EditText) findViewById(R.id.etUuid);
        uuid.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if(uuid.getText().toString().equals("")){
                        return false;
                    }
                    System.out.println("MIRA: "+uUID);
                    Log.i("COOL", "I ENTRO" + uUID);
                    changeUuid();
                }
                return false;
            }
        });
        range.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if(range.getText().toString().equals("")){
                        return false;
                    }
                    changeRange();
                    changeUuid();
                }
                return false;
            }
        });
        actualRange = (TextView) findViewById(R.id.tvActualRange);
        actualRange.setText(rangeTxt+" m");
        safeMode= (TextView) findViewById(R.id.tvSafety);
        tvDistance = (TextView) findViewById(R.id.tvDistance);
        if(range.getText() == null){
            rangeTxt = Integer.parseInt(range.getText().toString());
        }
        beaconManager = BeaconManager.getInstanceForApplication(this);
        // To detect proprietary beacons, you must add a line like below corresponding to your beacon
        // type.  Do a web search for "setBeaconLayout" to get the proper expression.
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
        beaconManager.bind(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check 
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This app needs location access");
                builder.setMessage("Please grant location access so this app can detect beacons.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });
                builder.show();
            }
    }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.unbind(this);
    }
    @Override
    public void onBeaconServiceConnect() {
        beaconManager.addMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(org.altbeacon.beacon.Region region) {
                Log.i(TAG, "I just saw an beacon for the first time!");
            }

            @Override
            public void didExitRegion(org.altbeacon.beacon.Region region) {
                Log.i(TAG, "I no longer see an beacon");
            }

            @Override
            public void didDetermineStateForRegion(int i, org.altbeacon.beacon.Region region) {
                Log.i(TAG, "I have just switched from seeing/not seeing beacons: " + i);
            }
        });

        try {
            beaconManager.startMonitoringBeaconsInRegion(new org.altbeacon.beacon.Region(uUID, null, null, null));
        } catch (RemoteException e) {
        }
        beaconManager.addRangeNotifier(new RangeNotifier() {

            @Override
            public void didRangeBeaconsInRegion(final Collection<Beacon> collection, org.altbeacon.beacon.Region region) {
                if (collection.size() > 0) {
                    Log.i(TAG, "The first beacon I see is about " + collection.iterator().next().getDistance() + " meters away.");
                    tvDistance.post(new Runnable() {
                        public void run() {
                            tvDistance.setText("Distance from child: "+ collection.iterator().next().getDistance());
                        }
                    });
                    if(collection.iterator().next().getDistance()>rangeTxt){
                        alertar(1);
                    }
                    else {
                        alertar(2);
                    }
                }
            }
        });

        try {
            beaconManager.startRangingBeaconsInRegion(new org.altbeacon.beacon.Region(uUID, null, null, null));
        } catch (RemoteException e) {}

    }
    public void alertar(int fuera) {

        if(fuera==1) {
            safeMode.post(new Runnable() {
                @Override
                public void run() {
                    safeMode.setText("OUT OF SAFE ZONE");
                    safeMode.setTextColor(Color.RED);
                }
            });
        }
        else {
            safeMode.post(new Runnable() {
                @Override
                public void run() {
                    safeMode.setText("IN SAFE ZONE");
                    safeMode.setTextColor(Color.GREEN);
                }
            });
        }
    }
    public void changeRange(){
        rangeTxt = Integer.parseInt(range.getText().toString());
        actualRange.setText(rangeTxt+" m");
    }
    public void changeUuid(){
        uUID = uuid.getText().toString();
        Log.i("COOL", "I have justaa" + uUID);
        onBeaconServiceConnect();
    }

}


