package com.example.boris.childmonitor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.boris.childmonitor.R;
import com.example.boris.childmonitor.clases.InfoHijo;
import com.example.boris.childmonitor.clases.ListaHijosDelPadre;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.math.BigDecimal;
import java.util.ArrayList;

public class InfoHijoActivity extends AppCompatActivity {

    ArrayList listaHijos;
    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mHijoRef = mRootRef.child("Hijos");
    FirebaseAuth firebaseAuth;
    ListaHijosDelPadre listaHijosDelPadre;
    String nombreHijo;
    InfoHijo info;
    TextView nombreH;
    TextView monitoreoH;
    TextView edadH;
    Button btnChildMode;
    String padre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_hijo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        listaHijos = new ArrayList();
        nombreHijo = intent.getStringExtra("name");
        padre = intent.getStringExtra("father");
        nombreH = (TextView) findViewById(R.id.tvNombre);
        monitoreoH = (TextView) findViewById(R.id.tvMonitoring);
        edadH = (TextView) findViewById(R.id.tvEdad);
        btnChildMode = (Button) findViewById(R.id.btnChildMode);
        btnChildMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(monitoreoH.getText().toString().equals("Beacon Monitoring")){
                    Intent inteBeacon = new Intent(InfoHijoActivity.this, BeaconActivity.class);
                    startActivity(inteBeacon);
                }
                else {
                    Intent inte = new Intent(InfoHijoActivity.this, ChildModeActivity.class);
                    startActivity(inte);
                }

            }
        });
    }
    public void getHijo(){
        nombreH.setText(info.getNombre());
        monitoreoH.setText(info.getMonitoreo());
        edadH.setText(info.getEdad()+"");
    }
    protected void onStart(){
        super.onStart();

        mHijoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                    for (DataSnapshot messagePadreSnapshot: messageSnapshot.getChildren()) {
                        if(messageSnapshot.getKey().equals(padre)) {

                            String name = (String) messagePadreSnapshot.child("nombre").getValue();
                            String father = (String) messagePadreSnapshot.child("padre").getValue();
                            String monitoreo=(String) messagePadreSnapshot.child("monitoreo").getValue();
                            long edad =(long) messagePadreSnapshot.child("edad").getValue();
                            int edadF= new BigDecimal(edad).intValueExact();
                            boolean smartphone= (boolean) messagePadreSnapshot.child("smartphone").getValue();
                            if (name.equals(nombreHijo)){
                                info = new InfoHijo(father,name,edadF,smartphone,monitoreo);
                            }
                        }
                    }
                }
                getHijo();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}
