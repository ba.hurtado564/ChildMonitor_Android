package com.example.boris.childmonitor.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.boris.childmonitor.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {
    private EditText email;
    private EditText password;
    private Button login;
    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Window w = getWindow();
        w.setTitle("My title");
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        FirebaseApp.initializeApp(this);
        firebaseAuth= FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser()!=null){
            Intent i = new Intent(Login.this,PerfilFinalActivity.class);
            startActivity(i);
        }
        email =(EditText)findViewById(R.id.email_text);
        password=(EditText)findViewById(R.id.contaseña_text);
        login=(Button)findViewById(R.id.log_in);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    Button singup = (Button)findViewById(R.id.sing_up);
        singup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                singUp();
            }
        });
    }

    private void login() {
        String emailU = email.getText().toString().trim();
        String passwordU = password.getText().toString().trim();

        if(TextUtils.isEmpty(emailU)){
            Toast.makeText(this,"Ingrese un email por favor",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(passwordU)){
            Toast.makeText(this,"Ingrese una contraseña por favor",Toast.LENGTH_SHORT).show();
            return;
        }
        firebaseAuth.signInWithEmailAndPassword(emailU,passwordU).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    //Comienza la sesion
                    finish();
                    Intent i = new Intent(Login.this,PerfilFinalActivity.class);
                    startActivity(i);
                }

            }
        });
    }

    private void singUp ()
    {

        Intent i = new Intent(this,SingupActivity.class);
        startActivity(i);

    }
    @Override
    protected void onResume(){
        if(firebaseAuth.getCurrentUser()!=null){

            Intent i = new Intent(Login.this,PerfilFinalActivity.class);
            startActivity(i);

        }

            super.onResume();




    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch (menuItem.getItemId()){
            case R.id.user_log_out:
                logOut();
                return true;
            default:return super.onOptionsItemSelected(menuItem);
        }

    }

    private void logOut() {
        firebaseAuth.signOut();

    }
}
