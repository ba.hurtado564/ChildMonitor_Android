package com.example.boris.childmonitor.services;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by GSN on 23/10/2017.
 */

public class RestClient {

        private static final String BASE_URL = "https://fcm.googleapis.com/fcm/";

        static AsyncHttpClient client = new AsyncHttpClient();

        public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
            client.get(getAbsoluteUrl(url), params, responseHandler);
        }

        public static void post(Context context, String url, StringEntity entity, String contentT, AsyncHttpResponseHandler responseHandler) {
            client.setURLEncodingEnabled(false);
            client.addHeader("Authorization", "key=AAAAw3ItiVg:APA91bGjo3WxKZffzuR8FBSQh1kOhf5OsdIKWapMbxZmt9pflvyBqpzTrf7AR6Yhg7FsGjSlLePW0R4Vpz2mjH7KqBiuGL_iwMVO5owzrEPvoj61xs3O0SqPSVS77xChDv2czN7pl6nW");
            client.post(context, getAbsoluteUrl(url), entity, contentT, responseHandler);
        }

        public static void put(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
            client.put(getAbsoluteUrl(url), params, responseHandler);
        }

        private static String getAbsoluteUrl(String relativeUrl) {
            return BASE_URL + relativeUrl;
        }

}
