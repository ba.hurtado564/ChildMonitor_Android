package com.example.boris.childmonitor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.boris.childmonitor.R;
import com.example.boris.childmonitor.clases.Informacion;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class PerfilActivity extends AppCompatActivity {

    private TextView email;
    private Button continuar;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private EditText nombreU,apellidoU,numeroU;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("ChildMonitor");
        firebaseAuth=FirebaseAuth.getInstance();

        databaseReference =FirebaseDatabase.getInstance().getReference();
        email=(TextView) findViewById(R.id.textViewUseemail);
        FirebaseUser user  = firebaseAuth.getCurrentUser();
        email.setText("Cuéntanos más sobre ti");
        nombreU=(EditText)findViewById(R.id.nombre_perfil);
        apellidoU=(EditText)findViewById(R.id.apellido_perfil);
        numeroU=(EditText)findViewById(R.id.numero_perfil);
        continuar=(Button)findViewById(R.id.button_contiuar);
        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarInfo();
            }
        });

    }

    private void guardarInfo() {
        String nombre = nombreU.getText().toString();
        String apellido = apellidoU.getText().toString();

        if(TextUtils.isEmpty(nombre)){
            Toast.makeText(this,"El campo de nombre no debe ser vacío",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(apellido)){
            Toast.makeText(this,"El campo del apellido no debe ser vacío",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(numeroU.getText().toString())){
            Toast.makeText(this,"El campo de número telefónico no debe ser vacío",Toast.LENGTH_SHORT).show();
            return;
        }
        Double numero = Double.parseDouble(numeroU.getText().toString());
        Informacion user = new Informacion(nombre,apellido,numero);
        FirebaseUser users  = firebaseAuth.getCurrentUser();
        databaseReference.child(new String("Usuarios")).child(users.getUid()).setValue(user);

        Toast.makeText(this,"Logrado",Toast.LENGTH_SHORT).show();
        finish();
        Intent i = new Intent(PerfilActivity.this,PerfilFinalActivity.class);
        startActivity(i);
    }
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch (menuItem.getItemId()){
            case R.id.user_log_out:
                logOut();
                return true;
            default:return super.onOptionsItemSelected(menuItem);
        }

    }

    private void logOut() {
        firebaseAuth.signOut();
        Intent i = new Intent(PerfilActivity.this,Login.class);
        startActivity(i);
    }
}
