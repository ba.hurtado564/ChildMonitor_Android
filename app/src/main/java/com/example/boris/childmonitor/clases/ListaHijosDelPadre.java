package com.example.boris.childmonitor.clases;

import java.util.ArrayList;

/**
 * Created by GSN on 8/10/2017.
 */

public class ListaHijosDelPadre {
    ArrayList hijos;
    public ListaHijosDelPadre(){
        hijos = new ArrayList();
    }
    public ArrayList getListaHijos(){
        return hijos;
    }
    public void actualizarHijos(ArrayList nHijos){
        hijos = nHijos;
    }
    public InfoHijo hijoPorNombre(String nombre){
        InfoHijo aDar = null;
        for(int i=0; i<hijos.size(); i++){
            InfoHijo aDar2 =  (InfoHijo) hijos.get(i);
            if(aDar2.getNombre().equals(nombre)){
                aDar = aDar2;
            }
        }
        return  aDar;
    }
}
