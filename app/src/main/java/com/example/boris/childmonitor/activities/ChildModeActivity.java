package com.example.boris.childmonitor.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.boris.childmonitor.R;
import com.example.boris.childmonitor.services.RestClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.appindexing.Action;
import com.google.firebase.appindexing.FirebaseUserActions;
import com.google.firebase.appindexing.builders.Actions;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ChildModeActivity extends AppCompatActivity {
    String emailU;
    EditText pass;
    ImageButton danger;
    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseAuth=FirebaseAuth.getInstance();
        setContentView(R.layout.child_mode_activity);
        Button btnBack = (Button) findViewById(R.id.btBackChildMode);
        pass = (EditText) findViewById(R.id.etParentPassword) ;
        danger = (ImageButton) findViewById(R.id.ibDanger);
        emailU = firebaseAuth.getCurrentUser().getEmail();
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack(pass.getText().toString());
            }
        });
        danger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modeDanger();
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


    }

    private void modeDanger() {
        JSONObject obj = new JSONObject();
        try {
            JSONObject objNot = new JSONObject();
            objNot.put("body", "ALERT! KID ON ALERT MODE!");
            objNot.put("title", "ALERT!");
            obj.put("to", FirebaseInstanceId.getInstance().getToken());
            obj.put("notification", objNot);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringEntity entity = null;
        try {
            entity = new StringEntity(obj.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        RestClient.post(null, "send", entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                try {
                    System.out.println("ON SUCCESS");

                } catch (Exception e) {
                    Log.d("PedidosActivity", e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable error) {
                try {
                    System.out.println("ON FAILURE" + statusCode + " "+ headers[0] + " REspuesta " + res );
                } catch (Exception e) {
                    Log.d("PedidosActivity", e.getMessage());
                }
            }
        });
    }

    public void goBack(String nPassword) {
        if(nPassword.equals("")){
            Context context = getApplicationContext();
            CharSequence text = "Wrong Password";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
        else {
            firebaseAuth.signInWithEmailAndPassword(emailU, nPassword).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        Intent inte = new Intent(ChildModeActivity.this, PerfilFinalActivity.class);
                        startActivity(inte);
                    }

                }
            });
        }

    }
    @Override
    public void onBackPressed()
    {

    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        return Actions.newView("ChildMode", "http://[ENTER-YOUR-URL-HERE]");
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        FirebaseUserActions.getInstance().start(getIndexApiAction());
    }

    @Override
    public void onStop() {

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        FirebaseUserActions.getInstance().end(getIndexApiAction());
        super.onStop();
    }
}
