package com.example.boris.childmonitor.activities;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


import com.example.boris.childmonitor.R;
import com.example.boris.childmonitor.clases.InfoHijo;
import com.example.boris.childmonitor.clases.Informacion;
import com.example.boris.childmonitor.clases.ListaHijosDelPadre;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.RemoteMessage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.StrictMath.toIntExact;

public class PerfilFinalActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private FirebaseAuth firebaseAuth;
    private FloatingActionButton agregarChino;
    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mHijoRef = mRootRef.child("Hijos");
    ArrayList listaHijos;
    ListaHijosDelPadre listaHijosDelPadre;
    ArrayList nombres;
    private ListView arregloHijo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_final);
        Intent intent = getIntent();
        listaHijos = new ArrayList();
        llenarListaHijos();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        firebaseAuth=FirebaseAuth.getInstance();
        listaHijosDelPadre = new ListaHijosDelPadre();
        agregarChino=(FloatingActionButton)findViewById(R.id.boton_agregar_hijo);
        arregloHijo=(ListView)findViewById(R.id.hijos_array);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        agregarChino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PerfilFinalActivity.this,AddChildActivity.class );
                startActivity(i);
            }
        });
        arregloHijo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent inte = new Intent(PerfilFinalActivity.this,InfoHijoActivity.class);
                inte.putExtra("name", ((InfoHijo)listaHijos.get(i)).getNombre());
                inte.putExtra("father", firebaseAuth.getCurrentUser().getUid());
                inte.putExtra("email", firebaseAuth.getCurrentUser().getEmail());
                startActivity(inte);
            }
        });


    }
    public void llenarListaHijos(){
        nombres = new ArrayList();
        for(int i=0; i<listaHijos.size(); i++){
            InfoHijo hijoNombre = (InfoHijo) listaHijos.get(i);
            nombres.add(hijoNombre.nombre);
            System.out.println(hijoNombre.nombre + nombres);
        }
        arregloHijo = (ListView)findViewById(R.id.hijos_array);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nombres );
        arregloHijo.setAdapter(arrayAdapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.perfil_final, menu);
        return true;
    }
    public void onMessageReceived(final RemoteMessage remoteMessage){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                String mensaje = remoteMessage.getNotification().getBody();
                Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.perfil_log_out) {
            logOut();
            return true;
        }
        if(id==R.id.perfil_complementar_info){
            Intent i = new Intent (this, PerfilActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }
    private void logOut() {
        firebaseAuth.signOut();
        Intent i = new Intent(PerfilFinalActivity.this,Login.class);
        startActivity(i);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    protected void onStart(){
        super.onStart();

        mHijoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String idPadre = firebaseAuth.getCurrentUser().getUid();
                for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                    for (DataSnapshot messagePadreSnapshot: messageSnapshot.getChildren()) {
                        if(messageSnapshot.getKey().equals(idPadre)) {

                            String name = (String) messagePadreSnapshot.child("nombre").getValue();
                            String father = (String) messagePadreSnapshot.child("padre").getValue();
                            String monitoreo=(String) messagePadreSnapshot.child("monitoreo").getValue();
                            long edad =(long) messagePadreSnapshot.child("edad").getValue();

                                int edadF= new BigDecimal(edad).intValueExact();
                            boolean smartphone= (boolean) messagePadreSnapshot.child("smartphone").getValue();



                            InfoHijo info = new InfoHijo(father,name,edadF,smartphone,monitoreo);
                            listaHijos.add(info);

                        }
                    }
                }
                llenarListaHijos();
                listaHijosDelPadre.actualizarHijos(listaHijos);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}